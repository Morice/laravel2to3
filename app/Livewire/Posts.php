<?php

namespace App\Livewire;

use Livewire\Component;
use Livewire\WithPagination;

class Posts extends Component
{
    use WithPagination;

    public function render()
    {
        return view('livewire.posts', [
            'posts' => \App\Models\Post::paginate(4),
        ]);
    }
}
