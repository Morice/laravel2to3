<div>
    <div class="grid grid-cols-1 gap-5">
        @forelse($posts as $post)
            <div class="border rounded p-3">
                {{ $post->id }}<br>
                {{ $post->title }}
            </div>
        @empty
            <div class="border rounded p-3">
                Empty
            </div>
        @endforelse
    </div>

    {{ $posts->links() }}
</div>
